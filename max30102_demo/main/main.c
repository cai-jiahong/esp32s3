#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "max30102.h"
#include "iic.h"


static const char *TAG = "main";

static void state_task(void *pvParameters);

void app_main(void)
{
    max30102_init();
    xTaskCreate(state_task, "state_task", 4096, NULL, 6, NULL);
}

// 回调函数
static void state_task(void *pvParameters)
{
    static char InfoBuffer[512] = {0};
    while (1)
    {
        vTaskList((char *)&InfoBuffer);
        printf("任务名      任务状态 优先级   剩余栈 任务序号\r\n");
        printf("\r\n%s\r\n", InfoBuffer);
        vTaskDelay(pdMS_TO_TICKS(2000));
    }
}
