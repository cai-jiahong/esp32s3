#ifndef _MAX30102_H
#define _MAX30102_H





#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_log.h"
#include "esp_err.h" 

#include "driver/i2c.h"
#include "driver/gpio.h"

void max30102_init();
void get_temp();




#endif // !_MAX30102_H