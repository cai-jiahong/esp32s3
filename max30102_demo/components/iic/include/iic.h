#ifndef _IIC_H
#define _IIC_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"

#define I2C_MASTER_SCL_IO 13        /*!< GPIO number used for I2C master clock */
#define I2C_MASTER_SDA_IO 14        /*!< GPIO number used for I2C master data  */
#define I2C_MASTER_NUM 0            /*!< I2C master i2c port number, the number of i2c peripheral interfaces available will depend on the chip */
#define I2C_MASTER_FREQ_HZ 400000   /*!< I2C master clock frequency */
#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_TIMEOUT_MS 1000


esp_err_t iic_init(void);
esp_err_t iic_write(uint8_t sensor_addr,uint8_t reg_addr, uint8_t* data, uint8_t length);
esp_err_t iic_read(uint8_t sensor_addr,uint8_t reg_addr, uint8_t* data, uint8_t length);


void iic_test(void);




#endif // !_IIC_H


