#ifndef _DELAY_H
#define _DELAY_H


void delay_us(int n);
void delay_ms(int ms);


#endif // !_DELAY_H
