#include <stdio.h>
#include "delay.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

void delay_us(int n)
{
    ets_delay_us(n);
}

void delay_ms(int ms)
{
    vTaskDelay(pdMS_TO_TICKS(ms));
}
