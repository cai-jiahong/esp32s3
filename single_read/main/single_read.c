#include <stdio.h>
#include <stdlib.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "dht11.h"
#include "ds18b20.h"
#include "OLED_IIC.h"
#include "wifi.h"
#include "iot_ali.h"
#include "fan.h"
#include "mq2.h"
#include "smogsensor.h"

void wifi_init(void);
void app_main(void)
{

    smog_init();
    dht11_init();
    ds18b20_init();
    wifi_init();
    oled_init_gpio();
    fan_init();
    mq2_init();
    linkkit_main();
    while (1)
    {

        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}
