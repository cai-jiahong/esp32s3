#include <stdio.h>
#include "fan.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"


#define FAN_GPIO GPIO_NUM_33

TaskFunction_t fan_handle = NULL;

int fan_state = 0;
int fan_state_buf = 1;

void fan_task(void *Pvparam)
{
    while (1)
    {
        if(fan_state_buf != fan_state)
        {
            fan_state_buf = fan_state;
            if(fan_state == 1)
            {
                gpio_set_level(FAN_GPIO ,1);
                printf("open fan\r\n");
            }
            else
            {
                gpio_set_level(FAN_GPIO ,0);
                printf("close fan\r\n");
            }
        }
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

void fan_init(void)
{
    gpio_reset_pin(FAN_GPIO);
    gpio_set_direction(FAN_GPIO, GPIO_MODE_OUTPUT);
    xTaskCreate(fan_task, "fan_task", 2048, NULL, 1, &fan_handle);
}
