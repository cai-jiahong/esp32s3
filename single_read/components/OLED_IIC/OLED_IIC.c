#include <stdio.h>
#include "OLED_IIC.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "oled.h"
#include "driver/gpio.h"
#include "string.h"

#include "ds18b20.h"
#include "dht11.h"
TaskHandle_t oled_handle = NULL;

void oled_task(void *pvparam)
{
    char txt[20];
    while (1)
    {
        sprintf(txt,"T:%.1f H:%d  ",T, H);
        oled_show_string(0, 0, txt, 12);
        oled_refresh_gram();    /* 更新显示 */
        // printf("oled response\r\n");
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

void oled_init_gpio(void)
{
    oled_init();
    
    xTaskCreate(oled_task, "oled_task", 4096, NULL, 2, &oled_handle);
}