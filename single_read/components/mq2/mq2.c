#include <stdio.h>
#include "mq2.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "fan.h"


#define MQ2_GPIO GPIO_NUM_21

int mq2_state;
int mq2_state_buf;
void mq2_task(void* arg)
{
    while(1)
    {
        if(gpio_get_level(MQ2_GPIO))
        {
            mq2_state = 1;
        }
        else
        {
            mq2_state = 0;
        }
        if(mq2_state_buf != mq2_state)
        {
            mq2_state_buf = mq2_state;
            if(mq2_state == 1)
            {
                fan_state = 1;
            }
            else
            {
                fan_state = 0;
            }
        }
        
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

void mq2_init(void)
{
    gpio_reset_pin(MQ2_GPIO);

    gpio_set_direction(MQ2_GPIO, GPIO_MODE_INPUT);
    xTaskCreate(mq2_task, "mq2_task", 2048, NULL, 1, NULL);
}