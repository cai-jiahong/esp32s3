#include <stdio.h>
#include "esp_log.h"
#include "driver/i2c.h"
#include "mpu6050.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "hal/i2c_hal.h"
#include "hal/gpio_hal.h"
#include "soc/i2c_periph.h"
#include "driver/i2c.h"
#include "ESP_log.h"
#include "string.h"
#include "stdlib.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"
#include "mpu6050.h"
#include "delay.h"
#include "math.h"

#define I2C_MASTER_SCL_IO 13        /*!< GPIO number used for I2C master clock */
#define I2C_MASTER_SDA_IO 14        /*!< GPIO number used for I2C master data  */
#define I2C_MASTER_NUM 0            /*!< I2C master i2c port number, the number of i2c peripheral interfaces available will depend on the chip */
#define I2C_MASTER_FREQ_HZ 400000   /*!< I2C master clock frequency */
#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_TIMEOUT_MS 1000

#define MPU6050_SENSOR_ADDR 0X68

#define delta_T 0.001f // 采样周期10ms 即频率100HZ

i2c_port_t i2c_master_port = I2C_MASTER_NUM;

static const char *TAG = "mpu6050";

typedef struct
{
    i2c_hw_cmd_t hw_cmd;
    union
    {
        uint8_t *data;     // When total_bytes > 1
        uint8_t data_byte; // when total_byte == 1
    };
    size_t bytes_used;
    size_t total_bytes;
} i2c_cmd_t;
typedef struct i2c_cmd_link
{
    i2c_cmd_t cmd;             /*!< command in current cmd link */
    struct i2c_cmd_link *next; /*!< next cmd link */
} i2c_cmd_link_t;

typedef struct
{
    i2c_cmd_link_t *head; /*!< head of the command link */
    i2c_cmd_link_t *cur;  /*!< last node of the command link */
    i2c_cmd_link_t *free; /*!< the first node to free of the command link */

    void *free_buffer;  /*!< pointer to the next free data in user's buffer */
    uint32_t free_size; /*!< remaining size of the user's buffer */
} i2c_cmd_desc_t;
#define I2C_TRANS_BUF_MINIMUM_SIZE (sizeof(i2c_cmd_desc_t) +                                                                             \
                                    sizeof(i2c_cmd_link_t) * 8) /* It is required to have allocate one i2c_cmd_desc_t per command:       \
                                                                 * start + write (device address) + write buffer +                       \
                                                                 * start + write (device address) + read buffer + read buffer for NACK + \
                                                                 * stop */

static esp_err_t mpu6050_i2c_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;

    i2c_config_t conf = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = I2C_MASTER_SDA_IO,
        .scl_io_num = I2C_MASTER_SCL_IO,
        .sda_pullup_en = GPIO_PULLUP_ENABLE,
        .scl_pullup_en = GPIO_PULLUP_ENABLE,
        .master.clk_speed = I2C_MASTER_FREQ_HZ,
    };

    i2c_param_config(i2c_master_port, &conf);

    return i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
}

float I_ex, I_ey, I_ez;               // 误差积分
mpu_param_t mpu_data;                 // MPU采集的六轴数值
quater_param_t Q_info = {1, 0, 0, 0}; // 四元数初始化
euler_param_t eulerAngle;             // 欧拉角
gyro_param_t GyroOffset;              // 陀螺仪校准值
float mpu_kp = 0.17;                  // 加速度计的收敛速率比例增益
float mpu_ki = 0.004;                 // 陀螺仪收敛速率的积分增益

void mpu6050_init_io(void)
{
    ESP_ERROR_CHECK(mpu6050_i2c_init());
    // ESP_LOGI("MPU6050", "I2C initialized successfully");
}

static esp_err_t mpu6050_register_write_byte(uint8_t reg_addr, uint8_t data)
{
    esp_err_t err = ESP_OK;
    uint8_t buffer[I2C_TRANS_BUF_MINIMUM_SIZE] = {0};
    uint8_t write_buf[2] = {reg_addr, data};

    i2c_cmd_handle_t handle = i2c_cmd_link_create_static(buffer, sizeof(buffer));
    assert(handle != NULL);

    err = i2c_master_start(handle);
    if (err != ESP_OK)
    {
        goto end;
    }

    err = i2c_master_write_byte(handle, MPU6050_SENSOR_ADDR << 1 | I2C_MASTER_WRITE, true);
    if (err != ESP_OK)
    {
        goto end;
    }

    err = i2c_master_write(handle, write_buf, sizeof(write_buf), true);
    if (err != ESP_OK)
    {
        goto end;
    }

    i2c_master_stop(handle);
    err = i2c_master_cmd_begin(i2c_master_port, handle, I2C_MASTER_TIMEOUT_MS / portTICK_RATE_MS);

end:
    i2c_cmd_link_delete_static(handle);
    return err;
}

static esp_err_t mpu6050_register_read(uint8_t reg_addr, uint8_t *data, size_t len)
{
    return i2c_master_write_read_device(I2C_MASTER_NUM, MPU6050_SENSOR_ADDR, &reg_addr, 1, data, len, I2C_MASTER_TIMEOUT_MS / portTICK_RATE_MS);
}

// 设置MPU6050陀螺仪传感器满量程范围
// fsr:0,±250dps;1,±500dps;2,±1000dps;3,±2000dps
// 返回值:0,设置成功
//     其他,设置失败
esp_err_t MPU_Set_Gyro_Fsr(unsigned char fsr)
{
    return mpu6050_register_write_byte(MPU_GYRO_CFG_REG, fsr << 3); // 设置陀螺仪满量程范围
}

// 设置MPU6050加速度传感器满量程范围
// fsr:0,±2g;1,±4g;2,±8g;3,±16g
// 返回值:0,设置成功
//     其他,设置失败
esp_err_t MPU_Set_Accel_Fsr(unsigned char fsr)
{
    return mpu6050_register_write_byte(MPU_ACCEL_CFG_REG, fsr << 3); // 设置加速度传感器满量程范围
}
// 设置MPU6050的数字低通滤波器
// lpf:数字低通滤波频率(Hz)
// 返回值:0,设置成功
//     其他,设置失败
esp_err_t MPU_Set_LPF(unsigned short lpf)
{
    unsigned char data = 0;
    if (lpf >= 188)
        data = 1;
    else if (lpf >= 98)
        data = 2;
    else if (lpf >= 42)
        data = 3;
    else if (lpf >= 20)
        data = 4;
    else if (lpf >= 10)
        data = 5;
    else
        data = 6;
    return mpu6050_register_write_byte(MPU_CFG_REG, data); // 设置数字低通滤波器
}
// 设置MPU6050的采样率(假定Fs=1KHz)
// rate:4~1000(Hz)
// 返回值:0,设置成功
//     其他,设置失败
esp_err_t MPU_Set_Rate(unsigned short rate)
{
    unsigned char data;
    if (rate > 1000)
        rate = 1000;
    if (rate < 4)
        rate = 4;
    data = 1000 / rate - 1;
    data = mpu6050_register_write_byte(MPU_SAMPLE_RATE_REG, data); // 设置数字低通滤波器
    return MPU_Set_LPF(rate / 2);                                  // 自动设置LPF为采样率的一半
}

// 得到温度值
// 返回值:温度值(扩大了100倍)
short MPU_Get_Temperature(void)
{
    unsigned char buf[2];
    short raw;
    float temp;

    ESP_ERROR_CHECK(mpu6050_register_read(MPU_TEMP_OUTH_REG, buf, sizeof(buf)));
    raw = ((unsigned short)buf[0] << 8) | buf[1];
    temp = 36.53 + ((double)raw) / 340;
    return temp * 100;
    ;
}

// 得到陀螺仪值(原始值)
// gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
// 返回值:0,成功
//     其他,错误代码
esp_err_t MPU_Get_Gyroscope(short *gx, short *gy, short *gz)
{
    unsigned char buf[6];
    esp_err_t res;

    res = mpu6050_register_read(MPU_GYRO_XOUTH_REG, buf, sizeof(buf));
    if (res == 0)
    {
        *gx = ((unsigned short)buf[0] << 8) | buf[1];
        *gy = ((unsigned short)buf[2] << 8) | buf[3];
        *gz = ((unsigned short)buf[4] << 8) | buf[5];
    }
    return res;
}
// 得到加速度值(原始值)
// gx,gy,gz:陀螺仪x,y,z轴的原始读数(带符号)
// 返回值:0,成功
//     其他,错误代码
esp_err_t MPU_Get_Accelerometer(short *ax, short *ay, short *az)
{
    unsigned char buf[6];
    esp_err_t res;
    res = mpu6050_register_read(MPU_ACCEL_XOUTH_REG, buf, 6);
    if (res == 0)
    {
        *ax = ((unsigned short)buf[0] << 8) | buf[1];
        *ay = ((unsigned short)buf[2] << 8) | buf[3];
        *az = ((unsigned short)buf[4] << 8) | buf[5];
    }
    return res;
}
esp_err_t mpu6050_init(void)
{
    esp_err_t res;
    mpu6050_init_io();
    ESP_ERROR_CHECK(mpu6050_register_write_byte(0x6B, 0x80)); // 复位MPU6050
    vTaskDelay(pdMS_TO_TICKS(100));
    ESP_ERROR_CHECK(mpu6050_register_write_byte(MPU_PWR_MGMT1_REG, 0X00)); // 唤醒MPU6050
    MPU_Set_Gyro_Fsr(3);                                                   // 陀螺仪传感器,±2000dps
    MPU_Set_Accel_Fsr(0);                                                  // 加速度传感器,±2g
    MPU_Set_Rate(50);                                                      // 设置采样率50Hz
    ESP_ERROR_CHECK(mpu6050_register_write_byte(MPU_INT_EN_REG, 0X00));    // 关闭所有中断
    ESP_ERROR_CHECK(mpu6050_register_write_byte(MPU_USER_CTRL_REG, 0X00)); // I2C主模式关闭
    ESP_ERROR_CHECK(mpu6050_register_write_byte(MPU_FIFO_EN_REG, 0X00));   // 关闭FIFO
    ESP_ERROR_CHECK(mpu6050_register_write_byte(MPU_INTBP_CFG_REG, 0X80)); // INT引脚低电平有效
    ESP_ERROR_CHECK(mpu6050_register_read(MPU_DEVICE_ID_REG, (uint8_t *)&res, sizeof(res)));

    if (res == MPU_ADDR) // 器件ID正确
    {
        ESP_ERROR_CHECK(mpu6050_register_write_byte(MPU_PWR_MGMT1_REG, 0X01)); // 设置CLKSEL,PLL X轴为参考
        ESP_ERROR_CHECK(mpu6050_register_write_byte(MPU_PWR_MGMT2_REG, 0X00)); // 加速度与陀螺仪都工作
        MPU_Set_Rate(50);
        ESP_LOGI(TAG, "init sucess \r\n"); // 设置采样率为50Hz
    }
    else
        return ESP_FAIL;

    return ESP_OK;
}

static esp_err_t mpu6050_register_write_byte_length(uint8_t reg_addr, uint8_t length, uint8_t const *data)
{
    esp_err_t err = ESP_OK;
    uint8_t buffer[I2C_TRANS_BUF_MINIMUM_SIZE] = {0};
    uint8_t write_buf[10];

    write_buf[0] = reg_addr;
    memcpy(write_buf + 1, data, length);

    i2c_cmd_handle_t handle = i2c_cmd_link_create_static(buffer, sizeof(buffer));
    assert(handle != NULL);

    err = i2c_master_start(handle);
    if (err != ESP_OK)
    {
        goto end;
    }

    err = i2c_master_write_byte(handle, MPU6050_SENSOR_ADDR << 1 | I2C_MASTER_WRITE, true);
    if (err != ESP_OK)
    {
        goto end;
    }

    err = i2c_master_write(handle, write_buf, length + 1, true);
    if (err != ESP_OK)
    {
        goto end;
    }

    i2c_master_stop(handle);
    err = i2c_master_cmd_begin(i2c_master_port, handle, I2C_MASTER_TIMEOUT_MS / portTICK_RATE_MS);

end:
    i2c_cmd_link_delete_static(handle);
    return err;
}

esp_err_t i2c_write(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data)
{
    return mpu6050_register_write_byte_length(reg_addr, length, data);
}

esp_err_t i2c_read(unsigned char slave_addr, unsigned char reg_addr, unsigned char length, unsigned char const *data)
{
    return mpu6050_register_read(reg_addr, (uint8_t *)data, length);
}

esp_err_t mpu6050_init_length(void)
{
    esp_err_t res;
    uint8_t buf[3];
    mpu6050_init_io();
    buf[0] = 0x80;
    ESP_ERROR_CHECK(mpu6050_register_write_byte_length(0x6B, 1, buf)); // 复位MPU6050
    vTaskDelay(pdMS_TO_TICKS(100));
    buf[0] = 0;
    ESP_ERROR_CHECK(mpu6050_register_write_byte_length(MPU_PWR_MGMT1_REG, 1, buf)); // 唤醒MPU6050
    MPU_Set_Gyro_Fsr(3);                                                            // 陀螺仪传感器,±2000dps
    MPU_Set_Accel_Fsr(0);                                                           // 加速度传感器,±2g
    MPU_Set_Rate(50);                                                               // 设置采样率50Hz
    ESP_ERROR_CHECK(mpu6050_register_write_byte_length(MPU_INT_EN_REG, 1, buf));    // 关闭所有中断
    ESP_ERROR_CHECK(mpu6050_register_write_byte_length(MPU_USER_CTRL_REG, 1, buf)); // I2C主模式关闭
    buf[0] = 0x01;
    ESP_ERROR_CHECK(mpu6050_register_write_byte_length(MPU_FIFO_EN_REG, 1, buf)); // 关闭FIFO
    buf[0] = 0x80;
    ESP_ERROR_CHECK(mpu6050_register_write_byte_length(MPU_INTBP_CFG_REG, 1, buf)); // INT引脚低电平有效
    ESP_ERROR_CHECK(mpu6050_register_read(MPU_DEVICE_ID_REG, (uint8_t *)&res, sizeof(res)));

    if (res == MPU_ADDR) // 器件ID正确
    {
        buf[0] = 0x01;
        ESP_ERROR_CHECK(mpu6050_register_write_byte_length(MPU_PWR_MGMT1_REG, 1, buf)); // 设置CLKSEL,PLL X轴为参考
        buf[0] = 0x00;
        ESP_ERROR_CHECK(mpu6050_register_write_byte_length(MPU_PWR_MGMT2_REG, 1, buf)); // 加速度与陀螺仪都工作
        MPU_Set_Rate(50);
        ESP_LOGI(TAG, "init success \r\n"); // 设置采样率为50Hz
    }
    else
        return ESP_FAIL;

    if (res == ESP_OK)
    {
        ESP_LOGI(TAG, "mpu6050 init success \r\n");
    }
    else
    {
        ESP_LOGI(TAG, "mpu6050 init fail \r\n");
    }
    return ESP_OK;
}

gyro_param_t GyroOffset; // 陀螺仪校准值
short mpu_gyro_x, mpu_gyro_y, mpu_gyro_z;
short mpu_acc_x, mpu_acc_y, mpu_acc_z;

void get_mpu6050_gyro(void)
{
    short gyrox, gyroy, gyroz;                 // 陀螺仪原始数据
    MPU_Get_Gyroscope(&gyrox, &gyroy, &gyroz); // 得到陀螺仪数据

    mpu_gyro_x = gyrox;
    mpu_gyro_y = gyroy;
    mpu_gyro_z = gyroz;
}

void get_mpu6050_accdata(void)
{
    short aacx, aacy, aacz;                     // 加速度传感器原始数据
    MPU_Get_Accelerometer(&aacx, &aacy, &aacz); // 得到加速度传感器数据

    mpu_acc_x = aacx;
    mpu_acc_y = aacy;
    mpu_acc_z = aacz;
}

void gyroOffsetInit(void)
{
    GyroOffset.Xdata = 0;
    GyroOffset.Ydata = 0;
    GyroOffset.Zdata = 0;
    for (uint16_t i = 0; i < 100; ++i)
    {
        get_mpu6050_gyro(); // 获取陀螺仪角速度
        GyroOffset.Xdata += mpu_gyro_x;
        GyroOffset.Ydata += mpu_gyro_y;
        GyroOffset.Zdata += mpu_gyro_z;
        vTaskDelay(pdMS_TO_TICKS(10)); // 最大 1Khz
    }

    GyroOffset.Xdata /= 100;
    GyroOffset.Ydata /= 100;
    GyroOffset.Zdata /= 100;
}

// 求倒数平方根
float myRsqrt(float num)
{
    float halfx = 0.5f * num;
    float y = num;
    long i = *(long *)&y;
    i = 0x5f375a86 - (i >> 1);

    y = *(float *)&i;
    y = y * (1.5f - (halfx * y * y));
    y = y * (1.5f - (halfx * y * y));

    return y;
}
/**
 * @brief 用互补滤波算法解算陀螺仪姿态(即利用加速度计修正陀螺仪的积分误差)
 * 加速度计对振动之类的噪声比较敏感，长期数据计算出的姿态可信；陀螺仪对振动噪声不敏感，短期数据可信，但长期使用积分误差严重(内部积分算法放大静态误差)。
 * 因此使用姿态互补滤波，短期相信陀螺仪，长期相信加速度计。
 * @tips: n - 导航坐标系； b - 载体坐标系
 */
void mpuAHRSupdate(mpu_param_t *mpu)
{
    float halfT = 0.5 * delta_T; // 采样周期一半
    float vx, vy, vz;            // 当前姿态计算得来的重力在三轴上的分量
    float ex, ey, ez;            // 当前加速计测得的重力加速度在三轴上的分量与用当前姿态计算得来的重力在三轴上的分量的误差

    float q0 = Q_info.q0; // 四元数
    float q1 = Q_info.q1;
    float q2 = Q_info.q2;
    float q3 = Q_info.q3;

    float q0q0 = q0 * q0; // 先相乘，方便后续计算
    float q0q1 = q0 * q1;
    float q0q2 = q0 * q2;
    float q0q3 = q0 * q3;
    float q1q1 = q1 * q1;
    float q1q2 = q1 * q2;
    float q1q3 = q1 * q3;
    float q2q2 = q2 * q2;
    float q2q3 = q2 * q3;
    float q3q3 = q3 * q3;

    // 正常静止状态为-g 反作用力。
    if (mpu->acc_x * mpu->acc_y * mpu->acc_z == 0) // 加计处于自由落体状态时(此时g = 0)不进行姿态解算，因为会产生分母无穷大的情况
        return;

    // 对加速度数据进行归一化 得到单位加速度 (a^b -> 载体坐标系下的加速度)
    float norm = myRsqrt(mpu->acc_x * mpu->acc_x + mpu->acc_y * mpu->acc_y + mpu->acc_z * mpu->acc_z);
    mpu->acc_x = mpu->acc_x * norm;
    mpu->acc_y = mpu->acc_y * norm;
    mpu->acc_z = mpu->acc_z * norm;

    // 载体坐标系下重力在三个轴上的分量
    vx = 2 * (q1q3 - q0q2);
    vy = 2 * (q0q1 + q2q3);
    vz = q0q0 - q1q1 - q2q2 + q3q3;

    // g^b 与 a^b 做向量叉乘，得到陀螺仪的校正补偿向量e的系数
    ex = mpu->acc_y * vz - mpu->acc_z * vy;
    ey = mpu->acc_z * vx - mpu->acc_x * vz;
    ez = mpu->acc_x * vy - mpu->acc_y * vx;

    // 误差累加
    I_ex += halfT * ex;
    I_ey += halfT * ey;
    I_ez += halfT * ez;

    // 使用PI控制器消除向量积误差(陀螺仪漂移误差)
    mpu->gyro_x = mpu->gyro_x + mpu_kp * ex + mpu_ki * I_ex;
    mpu->gyro_y = mpu->gyro_y + mpu_kp * ey + mpu_ki * I_ey;
    mpu->gyro_z = mpu->gyro_z + mpu_kp * ez + mpu_ki * I_ez;

    // 一阶龙格库塔法求解四元数微分方程，其中halfT为测量周期的1/2，gx gy gz为b系陀螺仪角速度。
    q0 = q0 + (-q1 * mpu->gyro_x - q2 * mpu->gyro_y - q3 * mpu->gyro_z) * halfT;
    q1 = q1 + (q0 * mpu->gyro_x + q2 * mpu->gyro_z - q3 * mpu->gyro_y) * halfT;
    q2 = q2 + (q0 * mpu->gyro_y - q1 * mpu->gyro_z + q3 * mpu->gyro_x) * halfT;
    q3 = q3 + (q0 * mpu->gyro_z + q1 * mpu->gyro_y - q2 * mpu->gyro_x) * halfT;

    // 单位化四元数在空间旋转时不会拉伸，仅有旋转角度，下面算法类似线性代数里的正交变换
    norm = myRsqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
    Q_info.q0 = q0 * norm;
    Q_info.q1 = q1 * norm;
    Q_info.q2 = q2 * norm;
    Q_info.q3 = q3 * norm; // 用全局变量记录上一次计算的四元数值
}

/**
 * @brief 将采集的数值转化为实际物理值, 并对陀螺仪进行去零漂处理
 * 加速度计初始化配置 -> 测量范围: ±8g        对应灵敏度: 4096 LSB/g
 * 陀螺仪初始化配置   -> 测量范围: ±2000 dps  对应灵敏度: 16.4 LSB/dps   (degree per second)
 * @tips: gyro = (gyro_val / 16.4) °/s = ((gyro_val / 16.4) * PI / 180) rad/s
 */
void mpuGetValues(void)
{
    float alpha = 0.3;

    //一阶低通滤波，单位g
    mpu_data.acc_x = (((float) mpu_acc_x) * alpha) / 4096 + mpu_data.acc_x * (1 - alpha);
    mpu_data.acc_y = (((float) mpu_acc_y) * alpha) / 4096 + mpu_data.acc_y * (1 - alpha);
    mpu_data.acc_z = (((float) mpu_acc_z) * alpha) / 4096 + mpu_data.acc_z * (1 - alpha);

    //! 陀螺仪角速度必须转换为弧度制角速度: deg/s -> rad/s
    mpu_data.gyro_x = ((float) mpu_gyro_x - GyroOffset.Xdata) * PI / 180 / 16.4f; 
    mpu_data.gyro_y = ((float) mpu_gyro_y - GyroOffset.Ydata) * PI / 180 / 16.4f;
    mpu_data.gyro_z = ((float) mpu_gyro_z - GyroOffset.Zdata) * PI / 180 / 16.4f;
}


void mpuGetEulerianAngles(void)
{
    //采集陀螺仪数据
    get_mpu6050_accdata();
    get_mpu6050_gyro();

    mpuGetValues();
    mpuAHRSupdate(&mpu_data);

    float q0 = Q_info.q0;
    float q1 = Q_info.q1;
    float q2 = Q_info.q2;
    float q3 = Q_info.q3;

    /* 四元数计算欧拉角 */
    // atan2返回输入坐标点与坐标原点连线与X轴正方形夹角的弧度值
    eulerAngle.pitch = asin(2 * q0 * q2 - 2 * q1 * q3) * 180 / PI; 
    eulerAngle.roll = atan2(2 * q2 * q3 + 2 * q0 * q1, -2 * q1 * q1 - 2 * q2 * q2 + 1) * 180 / PI; 
    eulerAngle.yaw = atan2(2 * q1 * q2 + 2 * q0 * q3, -2 * q2 * q2 - 2 * q3 * q3 + 1) * 180 / PI;  // 实际上加速度计对横滚角没有修正作用

    /* 姿态限制 */
    // if (eulerAngle.roll > 90 || eulerAngle.roll < -90) 
	// {
    //     if (eulerAngle.pitch > 0) 
	// 	{
    //         eulerAngle.pitch = 180 - eulerAngle.pitch;
    //     }
    //     if (eulerAngle.pitch < 0) 
	// 	{
    //         eulerAngle.pitch = -(180 + eulerAngle.pitch);
    //     }
    // }

    if (eulerAngle.yaw > 360) 
	{
        eulerAngle.yaw -= 360;
    } 
	else if (eulerAngle.yaw < 0) 
	{
        eulerAngle.yaw += 360;
    }

    ESP_LOGE(TAG, "eulerAngle yaw: %5.2f pitch: %5.2f roll: %5.2f", eulerAngle.yaw, eulerAngle.pitch, eulerAngle.roll);
}


void mpu6050_task(void *arg)
{
    mpu6050_init_length();
    gyroOffsetInit();

    while (1)
    {
        mpuGetEulerianAngles();
        // mpuGetValues();
        // mpuAHRSupdate(&mpu_data);
        vTaskDelay(pdMS_TO_TICKS(1));
    }
}
