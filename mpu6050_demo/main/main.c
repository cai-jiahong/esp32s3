#include <stdio.h>
#include "mpu6050.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"

static const char *TAG = "main";

void mpu6050_task(void *arg);
static void state_task(void *pvParameters);

void app_main(void)
{
    xTaskCreate(mpu6050_task, "mpu task", 2048, NULL, 0, NULL);
    // xTaskCreate(state_task, "state_task", 4096, NULL, 6, NULL);
}

// 回调函数
static void state_task(void *pvParameters)
{
    static char InfoBuffer[512] = {0};
    while (1)
    {
        vTaskList((char *)&InfoBuffer);
        printf("任务名      任务状态 优先级   剩余栈 任务序号\r\n");
        printf("\r\n%s\r\n", InfoBuffer);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}
